struct _ARG {
  int nbRot;
  int nbAtoms;
  int atypes[6];
  double charges[6];
  double coords[81][6][3];
};

struct _ASN {
  int nbRot;
  int nbAtoms;
  int atypes[3];
  double charges[3];
  double coords[18][3][3];
};

struct _ASP {
  int nbRot;
  int nbAtoms;
  int atypes[3];
  double charges[3];
  double coords[9][3][3];
};

struct _CYS {
  int nbRot;
  int nbAtoms;
  int atypes[1];
  double charges[1];
  double coords[3][1][3];
};

struct _GLN {
  int nbRot;
  int nbAtoms;
  int atypes[4];
  double charges[4];
  double coords[36][4][3];
};

struct _GLU {
  int nbRot;
  int nbAtoms;
  int atypes[4];
  double charges[4];
  double coords[27][4][3];
};

struct _HIS {
  int nbRot;
  int nbAtoms;
  int atypes[5];
  double charges[5];
  double coords[9][5][3];
};

struct _ILE {
  int nbRot;
  int nbAtoms;
  int atypes[3];
  double charges[3];
  double coords[9][3][3];
};

struct _LEU {
  int nbRot;
  int nbAtoms;
  int atypes[3];
  double charges[3];
  double coords[9][3][3];
};

struct _LYS {
  int nbRot;
  int nbAtoms;
  int atypes[4];
  double charges[4];
  double coords[81][4][3];
};

struct _MET {
  int nbRot;
  int nbAtoms;
  int atypes[3];
  double charges[3];
  double coords[27][3][3];
};

struct _PHE {
  int nbRot;
  int nbAtoms;
  int atypes[6];
  double charges[6];
  double coords[6][6][3];
};

struct _PRO {
  int nbRot;
  int nbAtoms;
  int atypes[2];
  double charges[2];
  double coords[2][2][3];
};

struct _SER {
  int nbRot;
  int nbAtoms;
  int atypes[1];
  double charges[1];
  double coords[3][1][3];
};

struct _THR {
  int nbRot;
  int nbAtoms;
  int atypes[2];
  double charges[2];
  double coords[3][2][3];
};

struct _TRP {
  int nbRot;
  int nbAtoms;
  int atypes[9];
  double charges[9];
  double coords[9][9][3];
};

struct _TYR {
  int nbRot;
  int nbAtoms;
  int atypes[7];
  double charges[7];
  double coords[6][7][3];
};

struct _VAL {
  int nbRot;
  int nbAtoms;
  int atypes[2];
  double charges[2];
  double coords[3][2][3];
};

